const path = require("path");
const vueSrc = "./src";

module.exports = {
  css: {
    loaderOptions: {
      sass: {
        additionalData: `@import "@/assets/scss/app.scss";`
      }
    }
  },
  chainWebpack: config => {
    config.module
      .rule("svg")
      .use("file-loader")
      .loader("svg-sprite-loader");

    config.module
      .rule("graphql")
      .test(/\.(graphql|gql)$/)
      .use("graphql-tag/loader")
      .loader("graphql-tag/loader")
      .end();
  }
};
