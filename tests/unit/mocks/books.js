export const MOCK_BOOKS = [
  {
    isbn: "c8fabf68-8374-48fe-a7ea-a00ccd07afff",
    title: "Henri Potier à l'école des sorciers",
    price: "35",
    cover:
      "https://firebasestorage.googleapis.com/v0/b/henri-potier.appspot.com/o/hp0.jpg?alt=media",
    synopsis:
      "Après la mort de ses parents (Lily et James Potier), Henri est recueilli par sa tante Pétunia (la sœur de Lily) et son oncle Vernon à l'âge d'un an. Ces derniers, animés depuis toujours d'une haine féroce envers les parents du garçon qu'ils qualifient de gens « bizarres », voire de « monstres », traitent froidement leur neveu et demeurent indifférents aux humiliations que leur fils Dudley lui fait subir. Henri ignore tout de l'histoire de ses parents, si ce n'est qu'ils ont été tués dans un accident de voiture",
    __typename: "BookType"
  },
  {
    isbn: "a460afed-e5e7-4e39-a39d-c885c05db861",
    title: "Henri Potier et la Chambre des secrets",
    price: "30",
    cover:
      "https://firebasestorage.googleapis.com/v0/b/henri-potier.appspot.com/o/hp1.jpg?alt=media",
    synopsis:
      "Henri Potier passe l'été chez les Dursley et reçoit la visite de Dobby, un elfe de maison. Celui-ci vient l'avertir que des évènements étranges vont bientôt se produire à Poudlard et lui conseille donc vivement de ne pas y retourner. Henri choisit d'ignorer cet avertissement. Le jour de son départ pour l'école, il se retrouve bloqué avec Ron Weasley à la gare de King's Cross, sans pouvoir se rendre sur le quai 9 ¾ où les attend le Poudlard Express. En dernier recours, les garçons se rendent donc à Poudlard à l'aide de la voiture volante de Monsieur Weasley et manquent de peu de se faire renvoyer dès leur arrivée à l'école pour avoir été aperçus au cours de leur voyage par plusieurs moldus.",
    __typename: "BookType"
  },
  {
    isbn: "fcd1e6fa-a63f-4f75-9da4-b560020b6acc",
    title: "Henri Potier et le Prisonnier d'Azkaban",
    price: "30",
    cover:
      "https://firebasestorage.googleapis.com/v0/b/henri-potier.appspot.com/o/hp2.jpg?alt=media",
    synopsis:
      "Durant l'été, pour son treizième anniversaire, Henri reçoit plusieurs cartes de ses amis, notamment une lettre de Ron qui lui écrit d'Égypte, où il passe ses vacances avec sa famille. Une lettre du professeur McGonagall, directrice adjointe de Poudlard, lui informe que les élèves de troisième année auront la possibilité de visiter le village de Pré-au-Lard.",
    __typename: "BookType"
  },
  {
    isbn: "c30968db-cb1d-442e-ad0f-80e37c077f89",
    title: "Henri Potier et la Coupe de feu",
    price: "29",
    cover:
      "https://firebasestorage.googleapis.com/v0/b/henri-potier.appspot.com/o/hp3.jpg?alt=media",
    synopsis:
      "Juste avant d'assister à la coupe du Monde de Quidditch opposant les équipes d'Irlande et de Bulgarie, Henri Potier fait un rêve étrange dans lequel il est témoin du meurtre d'un vieux jardinier moldu par Voldemort, alors que le jardinier surprenait une conversation au sujet de Henri.",
    __typename: "BookType"
  },
  {
    isbn: "78ee5f25-b84f-45f7-bf33-6c7b30f1b502",
    title: "Henri Potier et l'Ordre du phénix",
    price: "28",
    cover:
      "https://firebasestorage.googleapis.com/v0/b/henri-potier.appspot.com/o/hp4.jpg?alt=media",
    synopsis:
      "Pour faire face au retour de Voldemort, les membres de l'Ordre du phénix, sous la direction d'Albus Dumbledore, sont rassemblés au 12, Square Grimmaurd à Londres, leur quartier général. Le ministre de la magie ne croit pas au retour de Voldemort. La gazette du sorcier contribue à répandre ses idées, notamment en sous-entendant que Henri est un menteur ou que Dumbledore perd la raison.",
    __typename: "BookType"
  },
  {
    isbn: "cef179f2-7cbc-41d6-94ca-ecd23d9f7fd6",
    title: "Henri Potier et le Prince de sang-mêlé",
    price: "30",
    cover:
      "https://firebasestorage.googleapis.com/v0/b/henri-potier.appspot.com/o/hp5.jpg?alt=media",
    synopsis:
      "Henri rentre en sixième année à l'école de sorcellerie Poudlard. Il entre alors en possession d'un livre de potion portant le mot « propriété du Prince de sang-mêlé » et commence à en savoir plus sur le sombre passé de Voldemort qui était encore connu sous le nom de Tom Jedusor.",
    __typename: "BookType"
  },
  {
    isbn: "bbcee412-be64-4a0c-bf1e-315977acd924",
    title: "Henri Potier et les Reliques de la Mort",
    price: "35",
    cover:
      "https://firebasestorage.googleapis.com/v0/b/henri-potier.appspot.com/o/hp6.jpg?alt=media",
    synopsis:
      "Cette année, Henri a 17 ans et ne retourne pas à l'école de Poudlard après la mort de Dumbledore. Avec Ron et Hermione il se consacre à la dernière mission confiée par Dumbledore. Le Seigneur des Ténèbres règne en maître et traque les fidèles amis qui sont réduit à la clandestinité. D'épreuves en révélations, le courage, les choix et les sacrifices de Henri seront déterminants dans la lutte contre les forces du Mal.",
    __typename: "BookType"
  }
];

export const MOCK_FILTERED_BOOKS = [
  {
    isbn: "cef179f2-7cbc-41d6-94ca-ecd23d9f7fd6",
    title: "Henri Potier et le Prince de sang-mêlé",
    price: "30",
    cover:
      "https://firebasestorage.googleapis.com/v0/b/henri-potier.appspot.com/o/hp5.jpg?alt=media",
    synopsis:
      "Henri rentre en sixième année à l'école de sorcellerie Poudlard. Il entre alors en possession d'un livre de potion portant le mot « propriété du Prince de sang-mêlé » et commence à en savoir plus sur le sombre passé de Voldemort qui était encore connu sous le nom de Tom Jedusor.",
    __typename: "BookType"
  }
];
