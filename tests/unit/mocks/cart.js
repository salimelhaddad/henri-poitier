export const MOCK_COMMERCIAL_OFFERS = [
  { type: "percentage", value: 5 },
  { type: "minus", value: 15 },
  { type: "slice", sliceValue: 100, value: 12 }
];

export const MOCK_CART = [
  {
    isbn: "c8fabf68-8374-48fe-a7ea-a00ccd07afff",
    title: "Henri Potier à l'école des sorciers",
    price: {
      value: 35,
      formatted: "35 £"
    },
    cover:
      "https://firebasestorage.googleapis.com/v0/b/henri-potier.appspot.com/o/hp0.jpg?alt=media",
    synopsis:
      "Après la mort de ses parents (Lily et James Potier), Henri est recueilli par sa tante Pétunia (la sœur de Lily) et son oncle Vernon à l'âge d'un an. Ces derniers, animés depuis toujours d'une haine féroce envers les parents du garçon qu'ils qualifient de gens « bizarres », voire de « monstres », traitent froidement leur neveu et demeurent indifférents aux humiliations que leur fils Dudley lui fait subir. Henri ignore tout de l'histoire de ses parents, si ce n'est qu'ils ont été tués dans un accident de voiture"
  },
  {
    isbn: "fcd1e6fa-a63f-4f75-9da4-b560020b6acc",
    title: "Henri Potier et le Prisonnier d'Azkaban",
    price: {
      value: 30,
      formatted: "35 £"
    },
    cover:
      "https://firebasestorage.googleapis.com/v0/b/henri-potier.appspot.com/o/hp2.jpg?alt=media",
    synopsis:
      "Durant l'été, pour son treizième anniversaire, Henri reçoit plusieurs cartes de ses amis, notamment une lettre de Ron qui lui écrit d'Égypte, où il passe ses vacances avec sa famille. Une lettre du professeur McGonagall, directrice adjointe de Poudlard, lui informe que les élèves de troisième année auront la possibilité de visiter le village de Pré-au-Lard."
  },
  {
    isbn: "78ee5f25-b84f-45f7-bf33-6c7b30f1b502",
    title: "Henri Potier et l'Ordre du phénix",
    price: {
      value: 28,
      formatted: "28 £"
    },
    cover:
      "https://firebasestorage.googleapis.com/v0/b/henri-potier.appspot.com/o/hp4.jpg?alt=media",
    synopsis:
      "Pour faire face au retour de Voldemort, les membres de l'Ordre du phénix, sous la direction d'Albus Dumbledore, sont rassemblés au 12, Square Grimmaurd à Londres, leur quartier général. Le ministre de la magie ne croit pas au retour de Voldemort. La gazette du sorcier contribue à répandre ses idées, notamment en sous-entendant que Henri est un menteur ou que Dumbledore perd la raison."
  },
  {
    isbn: "cef179f2-7cbc-41d6-94ca-ecd23d9f7fd6",
    title: "Henri Potier et le Prince de sang-mêlé",
    price: {
      value: 30,
      formatted: "30 £"
    },
    cover:
      "https://firebasestorage.googleapis.com/v0/b/henri-potier.appspot.com/o/hp5.jpg?alt=media",
    synopsis:
      "Henri rentre en sixième année à l'école de sorcellerie Poudlard. Il entre alors en possession d'un livre de potion portant le mot « propriété du Prince de sang-mêlé » et commence à en savoir plus sur le sombre passé de Voldemort qui était encore connu sous le nom de Tom Jedusor."
  }
];
