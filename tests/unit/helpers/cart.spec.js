import { getBestPriceAfterDiscount, getTotalPrice } from "@/helpers/cart.js";

import { MOCK_COMMERCIAL_OFFERS, MOCK_CART } from "../mocks/cart.js";

describe("Given I have non empty cart", () => {
  describe("Given a client buys some books", () => {
    it("Should give him the best promotion of all available ones", () => {
      const availableOffers = MOCK_COMMERCIAL_OFFERS;
      const priceBeforeDiscount = 65;
      const discountedPrice = getBestPriceAfterDiscount(
        availableOffers,
        priceBeforeDiscount
      );
      const expected = {
        type: "minus",
        value: 50 // price after discount
      };
      expect(discountedPrice).toEqual(expected);
    });
  });

  describe("I want to display total price of the cart", () => {
    it("Should get the total amount", () => {
      const cart = MOCK_CART;
      const total = getTotalPrice(cart);
      const expected = 123;
      expect(total).toBe(expected);
    });
  });
});
