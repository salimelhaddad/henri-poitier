import { truncateText, getBooksIsbn } from "@/helpers/books.js";

import { MOCK_BOOKS } from "../mocks/books";

describe("Given I want to truncate a long text", () => {
  describe("Given I set 5 as the limit of characters", () => {
    it("Should get Only 5 characters followed by 3 dots", () => {
      const text = "Lorem ipsum dolor sit posuere."; // 30 character
      const truncated = truncateText(text, 5);
      const expected = "Lorem...";
      expect(truncated).toBe(expected);
    });
  });

  describe("Given I set no limit of characters", () => {
    it("Should get by default 20 characters followed by 3 dots", () => {
      const text = "Lorem ipsum dolor sit posuere."; // 30 character
      const truncated = truncateText(text);
      const expected = "Lorem ipsum dolor si...";
      expect(truncated).toBe(expected);
    });
  });
});

describe("Given I want to get a list of book's isbn", () => {
  it("Should get the list", () => {
    const isbnList = getBooksIsbn(MOCK_BOOKS);
    const expected = [
      "c8fabf68-8374-48fe-a7ea-a00ccd07afff",
      "a460afed-e5e7-4e39-a39d-c885c05db861",
      "fcd1e6fa-a63f-4f75-9da4-b560020b6acc",
      "c30968db-cb1d-442e-ad0f-80e37c077f89",
      "78ee5f25-b84f-45f7-bf33-6c7b30f1b502",
      "cef179f2-7cbc-41d6-94ca-ecd23d9f7fd6",
      "bbcee412-be64-4a0c-bf1e-315977acd924"
    ];
    expect(isbnList).toEqual(expected);
  });
});
