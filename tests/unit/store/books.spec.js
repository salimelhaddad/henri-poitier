import books from "@/store/modules/books";
import * as serviceBooks from "@/services/books";

import { MOCK_BOOKS, MOCK_FILTERED_BOOKS } from "../mocks/books";
import { MOCK_COMMERCIAL_OFFERS } from "../mocks/cart";

const actions = books.actions;

describe("Books Service", function() {
  // 2. scenario and 3. expectation
  it("Shoud get all available books", async () => {
    serviceBooks.filterBooksBy = jest.fn();
    const commit = jest.fn();
    serviceBooks.filterBooksBy.mockResolvedValue(MOCK_BOOKS);

    const books = await actions.filterBooksBy({ commit });
    expect(serviceBooks.filterBooksBy).toHaveBeenCalledTimes(1);
    expect(books.length).toBe(7);
    // expect(serviceBooks.getBooks.mock.calls[0][0]).toContain(`/books`)
  });

  describe("Given I want to search books by keyword", () => {
    it("Shoud get all books width title or synopsis including this keyword", async () => {
      serviceBooks.filterBooksBy = jest.fn();
      const commit = jest.fn();
      serviceBooks.filterBooksBy.mockResolvedValue(MOCK_FILTERED_BOOKS);

      const keyword = "Henri rentre en sixième année";
      const books = await actions.filterBooksBy({ commit }, keyword);
      const isKeywordFound = books.every(
        book => book.title.includes(keyword) || book.synopsis.includes(keyword)
      );
      expect(serviceBooks.filterBooksBy).toHaveBeenCalledTimes(1);
      expect(books.length).toBe(1);
      expect(isKeywordFound).toBeTruthy();
    });
  });

  describe("Given I have 2 books added to my cart", () => {
    it("Should get all available promotions", async () => {
      serviceBooks.getCommercialOffers = jest.fn();
      const commit = jest.fn();
      serviceBooks.getCommercialOffers.mockResolvedValue(
        MOCK_COMMERCIAL_OFFERS
      );

      const offers = await actions.getCommercialOffers({ commit });
      expect(serviceBooks.getCommercialOffers).toHaveBeenCalledTimes(1);
      expect(offers.length).toBe(3);
    });
  });
});
