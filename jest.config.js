module.exports = {
  preset: "@vue/cli-plugin-unit-jest",
  moduleFileExtensions: ["js", "json", "vue", "yml", "yaml"],
  transform: {
    "^.+\\.(vue)?$": "vue-jest",
    "\\.(gql|graphql)$": "jest-transform-graphql"
  }
};
