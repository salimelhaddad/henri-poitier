<p align="center">
  <a href="#">
    <img alt="henri-poitier" src="https://lh3.googleusercontent.com/proxy/3-GqExXeowiG4U__jiqYtXQvCF5JkSwtzD3kCsDkpfB0Sdh-67LH5nGh0NmRv5fSDWBROLsOWcbVQ-iDGPu4EzVWpox3UbPjyH2FPIYLok7blmnJMNhQ693qDfiz1mZ_x7d4ZWJTHekI" width="300" />
  </a>
</p>

<h1 align="center">
  Bienvenue à la librairie d'Henri-Poitier. Des livres qui coupent le souffle. Vous pouvez y accéder via le lien ci-dessous :
    https://henri-poitier.herokuapp.com
</h1>


## 🚅  Guide d'installation

1.  **Installer redis-server **
    L'installation de redis permet de mettre les appels déjà faits en cache, améliorant ainsi le temps de réponse.
    Sur Ubuntu/mac : Ouvrir le terminal et taper la commande suivante:
    ```shell
    # Clone the template
    sudo apt-get install redis-server
    redis-server
    ```

1.  **Démarrer le serveur Graphql**

    ```shell
    # Cloner le repository
    git clone https://gitlab.com/salimelhaddad/henri-poitier
    # Aller au dossier cloné
    cd henri-poitier/graphql-server
    # Installer les dépendences
    yarn
    # Démarrer le serveur
    yarn dev
    ```

1.  **Démarrer l'application front end**

    ```shell
    # Aller au dossier henri-poitier
    cd henri-poitier

    # copy .env file
    cp .env .env.dev

    # uncomment localhost and comment corvus-graphql
    VUE_APP_GRAPHQL_HTTP=http://localhost:4000/gql
    # VUE_APP_GRAPHQL_HTTP=https://corvus-grapqhl.herokuapp.com/gql

    # Installer les dépendences
    yarn
    
    # Démarrer l'application
    yarn dev
    ```

1.  **Démarrer le storybook your stories!**

     ```shell
    # Aller au dossier henri-poitier
    cd henri-poitier
    # Démarrer le storybook
    yarn storybook
    ```

## 🔎 What's inside?

A quick look at the top-level files and directories included with this template.

    .
    ├── .storybook
    ├── node_modules
    ├── public
    ├── src
        ├──  assets
        ├──  components
        ├──  graphql
        ├──  helpers
        ├──  pages
        ├──  plugins
        ├──  services
        ├──  store
        ├──  stories
        ├──  app.vue
        ├──  index.css
        ├──  main.js
        ├──  router.js
    ├── tests
        ├──  unit
            ├──  helpers
            ├──  mocks
            ├──  store
    ├── .browserslistrc
    ├── .eslintrc.js
    ├── .gitignore
    ├── babel.config.js
    ├── jest.config.js
    ├── LICENSE
    ├── yarn.lock
    ├── package.json
    └── README.md


1.  **`.storybook`**: This directory contains Storybook's [configuration](https://storybook.js.org/docs/react/configure/overview) files.

2.  **`node_modules`**: This directory contains all of the modules of code that your project depends on (npm packages).

3.  **`public`**: This directory will contain the development and production build of the site.

4.  **`src`**: This directory will contain all of the code related to what you will see on your application.

5.  **`tests`**: This directory will contain all of unit test files for your project.

6.  **`.browserslistrc`**: This file will contain the information regarding which browsers will be supported when the application is built.

7.  **`.eslintrc.js`**: This file contains the linting configuration for [eslint](https://eslint.org/).

8.  **`.gitignore`**: This file tells git which files it should not track or maintain during the development process of your project.

9.  **`.babel.config.js`**: This file tells [babel](https://babeljs.io/) how to transpile the application's code.

10.  **`jest.config.js`**: This is a configuration file for [Jest](https://jestjs.io/).

11. **`LICENSE`**: The template is licensed under the MIT licence.

12. **`package.json`**: Standard manifest file for Node.js projects, which typically includes project specific metadata (such as the project's name, the author among other information). It's based on this file that npm will know which packages are necessary to the project.

13. **`yarn.lock`**: This is an automatically generated file based on the exact versions of your npm dependencies that were installed for your project. **(Do not change it manually).**

14. **`README.md`**: A text file containing useful reference information about the project.
