import booksQuery from "@/graphql/books/books.gql";
import commercialOffersQuery from "@/graphql/books/commercialOffers.gql";

export const filterBooksBy = async (apolloClient, keyword) => {
  const { data } = await apolloClient.query({
    errorPolicy: "all",
    fetchPolicy: "network-only",
    variables: {
      keyword
    },
    query: booksQuery
  });
  return data && data.books;
};

export const getCommercialOffers = async (apolloClient, bookRefs) => {
  const { data } = await apolloClient.query({
    errorPolicy: "all",
    fetchPolicy: "network-only",
    variables: {
      bookRefs
    },
    query: commercialOffersQuery
  });
  return data && data.commercialOffers;
};
