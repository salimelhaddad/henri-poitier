import Vue from "vue";
import { BootstrapVue } from "bootstrap-vue";

// Import Bootstrap an BootstrapVue CSS files (order is important)
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue/dist/bootstrap-vue.css";

console.log(process.env.VUE_APP_GRAPHQL_HTTP);

// Make BootstrapVue available throughout your project
Vue.use(BootstrapVue);

import App from "./App.vue";
import store from "./store";
import apolloProvider from "./plugins/vue-apollo";
import router from "./router";

Vue.config.productionTip = false;

new Vue({
  apolloProvider,
  router,
  store,
  render: h => h(App)
}).$mount("#app");
