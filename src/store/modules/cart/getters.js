export default {
  cart: state => state.cart,
  priceBeforeDiscount: state => state.priceBeforeDiscount,
  discountedPrice: state => state.discountedPrice
};
