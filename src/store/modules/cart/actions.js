import { SET_CART, SET_PRICE_BEFORE_DISCOUNT } from "./types";
import { getTotalPrice } from "@/helpers/cart";
/* eslint-disable no-unused-vars*/
export default {
  async getCart({ commit, getters }) {
    return getters.cart;
  },
  async deleteItem({ commit, getters }, item) {
    const updatedCart = getters.cart.filter(e => e.isbn !== item.isbn);
    const basePrice = getTotalPrice(updatedCart);
    commit(SET_PRICE_BEFORE_DISCOUNT, basePrice);
    commit(SET_CART, updatedCart);
  }
};
