import {
  SET_CART,
  SET_PRICE_BEFORE_DISCOUNT,
  SET_DISCOUNTED_PRICE
} from "./types";

export default {
  [SET_CART](state, payload) {
    const currentState = state;
    currentState.cart = payload;
  },
  [SET_PRICE_BEFORE_DISCOUNT](state, payload) {
    const currentState = state;
    currentState.priceBeforeDiscount = payload;
  },
  [SET_DISCOUNTED_PRICE](state, payload) {
    const currentState = state;
    currentState.discountedPrice = payload;
  }
};
