import getters from "./getters";
import actions from "./actions";
import mutations from "./mutations";

const state = () => ({
  cart: [],
  priceBeforeDiscount: null,
  discountedPrice: {}
});

export default {
  namespaced: true,
  actions,
  state,
  getters,
  mutations
};
