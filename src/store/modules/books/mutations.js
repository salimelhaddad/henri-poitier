import { SET_BOOKS, SET_COMMERCIAL_OFFERS } from "./types";

export default {
  [SET_BOOKS](state, payload) {
    const currentState = state;
    currentState.books = payload;
  },
  [SET_COMMERCIAL_OFFERS](state, payload) {
    const currentState = state;
    currentState.commercialOffers = payload;
  }
};
