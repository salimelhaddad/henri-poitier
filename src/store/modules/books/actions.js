import { apolloClient } from "@/plugins/vue-apollo";
import { filterBooksBy, getCommercialOffers } from "@/services/books";

/* eslint-disable no-unused-vars*/
export default {
  async filterBooksBy({ commit }, keyword) {
    try {
      const books = await filterBooksBy(apolloClient, keyword);
      return books;
    } catch (err) {
      console.log(err);
    }
  },
  async getCommercialOffers({ commit }, bookRefs) {
    try {
      const offers = await getCommercialOffers(apolloClient, bookRefs);
      return offers;
    } catch (err) {
      console.log(err);
    }
  }
};
