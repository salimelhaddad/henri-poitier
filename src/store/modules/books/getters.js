export default {
  books: state => state.books,
  commercialOffers: state => state.commercialOffers
};
