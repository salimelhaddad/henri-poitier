export const truncateText = (text, limit = 20) => {
  const truncated = text.substr(0, limit);
  return truncated.length < text.length ? truncated + "..." : truncated;
};

export const getBooksIsbn = cart => cart.map(item => item.isbn);
