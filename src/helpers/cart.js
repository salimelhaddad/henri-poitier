const getDiscountedPriceByPercentageType = (percentage, price) => ({
  type: "percentage",
  value: price - (price * percentage) / 100
});

const getDiscountedPriceByMinusType = (minusValue, price) => ({
  type: "minus",
  value: price - minusValue
});

const getDiscountedPriceBySliceType = (sliceValue, discountValue, price) => {
  const slices = Math.floor(price / sliceValue);
  return {
    type: "slice",
    value: price - discountValue * slices
  };
};

export const getBestPriceAfterDiscount = (offers, basePrice) => {
  const discountedPrices = offers.map(offer => {
    if (offer.type === "percentage")
      return getDiscountedPriceByPercentageType(offer.value, basePrice);
    if (offer.type === "minus")
      return getDiscountedPriceByMinusType(offer.value, basePrice);
    if (offer.type === "slice")
      return getDiscountedPriceBySliceType(
        offer.sliceValue,
        offer.value,
        basePrice
      );
  });
  const min = Math.min.apply(
    Math,
    discountedPrices.map(e => e.value)
  );
  console.log(min, "min");
  return discountedPrices.find(price => price.value === min);
};

export const formatPrice = price => `${price} €`;

export const getTotalPrice = cart =>
  cart.reduce((acc, item) => acc + item.price.value, 0);
