import NavigationBar from "@/components/organisms/NavigationBar.vue";

export default {
  title: "Organisms/NavigationBar",
  component: NavigationBar
};

const Template = args => ({
  components: { NavigationBar },
  template: `
    <navigation-bar class="py-3" />
  `,
  args: {
    label: "Barre de recherche"
  }
});

export const Basic = Template.bind({});
Basic.args = {
  label: "Barre de recherche"
};
