import { storiesOf } from "@storybook/vue";
import { withKnobs, text, select, color } from "@storybook/addon-knobs";
import BookItem from "@/components/molecules/BookItem.vue";

const book = {
  isbn: "c8fabf68-8374-48fe-a7ea-a00ccd07afff",
  title: "Henri Potier à l'école des sorciers",
  price: "35",
  cover:
    "https://firebasestorage.googleapis.com/v0/b/henri-potier.appspot.com/o/hp0.jpg?alt=media",
  synopsis:
    "Après la mort de ses parents (Lily et James Potier), Henri est recueilli par sa tante Pétunia (la sœur de Lily) et son oncle Vernon à l'âge d'un an. Ces derniers, animés depuis toujours d'une haine féroce envers les parents du garçon qu'ils qualifient de gens « bizarres », voire de « monstres », traitent froidement leur neveu et demeurent indifférents aux humiliations que leur fils Dudley lui fait subir. Henri ignore tout de l'histoire de ses parents, si ce n'est qu'ils ont été tués dans un accident de voiture",
  __typename: "BookType"
};

storiesOf("Organisms", module)
  .addDecorator(withKnobs)
  .add("BookItem", () => ({
    components: { BookItem },
    props: {
      book: {
        type: Object,
        default: book
      }
    },
    template: `
      <div>
        <book-item
          :book="book" />
      </div>
    `
  }));
