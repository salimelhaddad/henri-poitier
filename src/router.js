import Vue from "vue";
import Router from "vue-router";
import BooksPage from "@/pages/BooksPage.vue";
import BookDetailPage from "@/pages/BookDetailPage.vue";
import BasketDetailsPage from "@/pages/BasketDetailsPage.vue";
import EmptyBasketPage from "@/pages/EmptyBasketPage.vue";

Vue.use(Router);

export default new Router({
  mode: "history",
  routes: [
    { path: "/", redirect: "/books" },
    {
      name: "Books",
      path: "/books",
      component: BooksPage
    },
    {
      name: "BookDetail",
      path: "/books/:id",
      component: BookDetailPage
    },
    {
      name: "BaksetDetails",
      path: "/my-basket",
      component: BasketDetailsPage
    },
    {
      name: "EmptyBasket",
      path: "/empty-basket",
      component: EmptyBasketPage
    }
  ]
});
