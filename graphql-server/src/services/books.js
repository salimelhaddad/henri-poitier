/* eslint-disable class-methods-use-this */
import { RESTDataSource } from 'apollo-datasource-rest';


export default class BooksApi extends RESTDataSource {
  constructor() {
    super();
    this.baseURL = process.env.BOOKS;
  }

  getBooks() {
    return this.get(`${this.baseURL}/books`);
  }

  getCommercialOffers(bookRefs) {
    return this.get(`${this.baseURL}/books/${bookRefs}/commercialOffers`);
  }
}
