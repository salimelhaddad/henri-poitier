import { gql } from 'apollo-server-express';


const typeDefs = gql`

  type PriceType {
    value: Int
    formatted: String
  }

  type BookType {
    isbn: String
    title: String
    price: PriceType
    cover: String
    synopsis: String
  }

  type CommercialOfferType {
    type: String
    value: Int
    sliceValue: Int
  }

  extend type Query {
    books(keyword: String): [BookType]
    commercialOffers(bookRefs: [String!]!): [CommercialOfferType]
  }
`;

export default typeDefs;
