export const formatBooks = (books) => books.map((book) => ({
  ...book,
  price: {
    value: book.price,
    formatted: `${book.price} €`,
  },
  synopsis: book.synopsis[0] || '',
}));

export const filterBooksBy = (keyword, books) => books
  .filter((book) => book.title.toLowerCase().includes(keyword.toLowerCase())
    || book.synopsis[0].toLowerCase().includes(keyword.toLowerCase()));

export const concatenateBookRefs = (bookRefs) => bookRefs.join(',');
