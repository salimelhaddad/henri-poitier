import { formatBooks, filterBooksBy, concatenateBookRefs } from './formatters';

const resolverMap = {
  Query: {
    async books(obj, { keyword }, { dataSources }) {
      try {
        const books = await dataSources
          .booksApi
          .getBooks();
        if (keyword) {
          const filtered = filterBooksBy(keyword, books);
          return formatBooks(filtered);
        }
        return formatBooks(books);
      } catch (e) {
        console.log(e, 'error');
      }
      return [];
    },
    async commercialOffers(obj, { bookRefs }, { dataSources }) {
      const concatenated = concatenateBookRefs(bookRefs);
      try {
        const { offers } = await dataSources.booksApi.getCommercialOffers(concatenated);
        return offers;
      } catch (e) {
        console.log(e, 'error');
      }
      return [];
    },
  },
};
export default resolverMap;
