const path = require('path');

module.exports = {
  resolve: {
    alias: {
      vue: 'vue/dist/vue.js'
    }
  },
  webpackFinal: async (config, { configType }) => {
    // `configType` has a value of 'DEVELOPMENT' or 'PRODUCTION'
    // You can change the configuration based on that.
    // 'PRODUCTION' is used when building the static version of storybook.

    // Make whatever fine-grained changes you need

    const rules = config.module.rules;

    rules.push({
      test: /\.scss$/,
      use: ['style-loader', 'css-loader', 'sass-loader', {
        loader: 'sass-resources-loader',
        options: {
          resources: [
            path.resolve(__dirname, '../src/assets/scss/colors.scss'),
            path.resolve(__dirname, '../src/assets/scss/functions.scss'),
            path.resolve(__dirname, '../src/assets/scss/mixins.scss')
          ]
        }
      }]
    });

    rules.push({
      test: /\.(ttf|woff|woff2|eot?)$/,
      loader: 'file-loader',
      include: path.resolve(__dirname, '../src/assets/fonts'),
    });

    rules.push({
      test: /\.svg/,
      loader: 'svg-sprite-loader',
      include: path.resolve(__dirname, '../src/assets/icon/svg'),
    });


    Object.assign(config.resolve.alias, {
      '@': path.resolve(__dirname, '../src/')
    });

    config.resolve.modules = [
      path.resolve(__dirname, '../src'),
      path.resolve(__dirname, '../src/stories'),
      'node_modules',
    ];

    config.module.rules.push({
      test: /\.scss$/,
      use: ['style-loader', 'css-loader', 'sass-loader'],
      include: path.resolve(__dirname, '../'),
    });

   config.resolve.extensions.push(".js", ".jsx");

    // Return the altered config
    return config;
  },
  "stories": [
    "../src/**/*.stories.mdx",
    "../src/**/*.stories.@(js|jsx|ts|tsx)"
  ],
  "addons": [
    "@storybook/addon-links",
    "@storybook/addon-essentials",
    "@storybook/addon-knobs"
  ]
}
